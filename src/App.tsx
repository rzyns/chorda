import React from "react";
import "./App.css";

// import Vex from "vexflow";

import SheetMusic from "@slnsw/react-sheet-music";

// const MyComponent: React.FunctionComponent<{ abc: string }> = (props) => {
//   return (
//     <SheetMusic
//       // Textual representation of music in ABC notation
//       // The string below will show four crotchets in one bar
//       notation="|EGBF|"
//     />
//   );
// };

const App: React.FunctionComponent = (a) => {
  // const VF = Vex.Flow;
  // const div = React.useRef<HTMLDivElement>(null);

  // React.useEffect(() => {
  //   const el = div.current;
  //   if (el) {
  //     const renderer = new VF.Renderer(el, VF.Renderer.Backends.SVG);
  //     // Configure the rendering context.
  //     renderer.resize(500, 500);
  //     const context = renderer.getContext();
  //     context.setFont("Arial", 10).setBackgroundFillStyle("#eed");

  //     // Create a stave of width 400 at position 10, 40 on the canvas.
  //     const stave = new VF.Stave(10, 40, 400);

  //     // Add a clef and time signature.
  //     stave.addClef("treble");
  //     // stave.addTimeSignature("4/4");

  //     // Connect it to the rendering context and draw!
  //     stave.setContext(context).draw();

  //     var notes = [
  //       // A quarter-note C.
  //       new VF.StaveNote({ clef: "treble", keys: ["c/4"], duration: "q" }),

  //       // A quarter-note D.
  //       new VF.StaveNote({ clef: "treble", keys: ["d/4"], duration: "q" }),

  //       // A quarter-note rest. Note that the key (b/4) specifies the vertical
  //       // position of the rest.
  //       new VF.StaveNote({ clef: "treble", keys: ["b/4"], duration: "qr" }),

  //       // A C-Major chord.
  //       new VF.StaveNote({
  //         clef: "treble",
  //         keys: ["c#/4", "e/4", "g/4"],
  //         duration: "q",
  //       }).addAccidental(0, new VF.Accidental("#")),
  //     ];

  //     // Create a voice in 4/4 and add the notes from above
  //     const voice = new VF.Voice({ num_beats: 4, beat_value: 4 });
  //     voice.addTickables(notes);

  //     // Format and justify the notes to 400 pixels.
  //     const formatter = new VF.Formatter();
  //     formatter.joinVoices([voice]).format([voice], 400);

  //     // Render voice
  //     voice.draw(context, stave);

  //     return function cleanup() {
  //       el.innerHTML = "";
  //     };
  //   }
  // });


  // Creating a custom hook
  function useInput(defaultValue: string) {
    const [value, setValue] = React.useState(defaultValue);
    function onChange(e: React.FormEvent<HTMLInputElement>) {
      if (e) {
        setValue(e.currentTarget.value);
      }
    }
    return {
      value,
      onChange,
    };
  }

  const inputProps = useInput("|ABCD|\n");

      // {/* <div ref={div}></div> */}
      // {/* <MyComponent abc={inputProps?.value ?? ""} /> */}
  return (
    <div className="App">
      <input type="text" {...inputProps} />
      <SheetMusic bpm={120} notation={inputProps.value} />
    </div>
  );
}

export default App;
